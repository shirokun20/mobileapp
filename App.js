import React from 'react';
import { LogBox, Platform } from 'react-native';
// Untuk menghilangkan warning di aplikasi tapi akan ada di log
//
import Core from './src'
export default function App() {

  React.useEffect(() => {
    if (Platform.OS !== 'web') LogBox.ignoreAllLogs();
  }, [])

  return (
      <Core />
  );
}
