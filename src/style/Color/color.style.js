const warna = {
    blue: '#1F99CC',
    red: '#CC1F1F',
    green: '#1CBC18',
    orange: '#FF7B40',
    grey: '#7D8797',
    black: '#3D3E3B',
    borderColor: '#E9E9E9',
    white: '#fff',
    transparent: 'transparent',
    appGreenColor: '#00D278',
    appGreen2Color: '#00B198',
}

module.exports = warna;