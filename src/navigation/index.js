import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
//
import { BottomTabScreen } from './../screens'


const {
    Navigator,
    Screen
} = createStackNavigator();

const NeedLogin = () => (
    <Screen
        name="bottomTabScreen"
        options={{
            headerShown: false,
        }}
        component={BottomTabScreen}
    />
)

const Navigation = () => {
    return (
        <Navigator>
            {NeedLogin()}
        </Navigator>
    )
};

export default Navigation;
