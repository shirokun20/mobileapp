import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
//
import { AddNewsScreen, HomeScreen, LoginScreen, NewsScreen, SplashScreen, TodoApps } from '../screens'
import { warna } from '../style';
import { TouchableOpacity, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RootContext } from '../Context';

const {
    Navigator,
    Screen
} = createStackNavigator();

const NeedLogin = () => (
    <>
        <Screen
            name="homeScreen"
            options={{
                headerShown: false,
            }}
            component={HomeScreen}
        />
        <Screen
            name="TodoApps"
            options={{
                headerTitle: 'Todo App Latihan',
            }}
            component={TodoApps}
        />
        <Screen
            name="NewsScreen"
            options={({ navigation, route }) => ({
                headerTitle: 'News Latihan',
                headerRight: () => {
                    return (
                        <TouchableOpacity style={{
                            backgroundColor: warna.red,
                            paddingVertical: 10,
                            paddingHorizontal: 5,
                            alignItems: 'center',
                            borderRadius: 10,
                            marginRight: 10,
                            elevation: 5,
                        }} onPress={() => navigation.navigate('AddNewsScreen')}>
                            <Text style={{
                                color: warna.white,
                                fontWeight: 'bold'
                            }}>Add News</Text>
                        </TouchableOpacity>
                    )
                }
            })}
            component={NewsScreen}
        />
        <Screen
            name="AddNewsScreen"
            options={{
                headerTitle: 'Tambah News',
            }}
            component={AddNewsScreen}
        />
    </>
)

const WithoutLogin = () => (
    <Screen
        name="loginScreen"
        options={{
            headerShown: false,
        }}
        component={LoginScreen}
    />
)

const Navigation = ({
    params,
}) => {
    const [token, setToken] = React.useState('');
    const [loading, setLoading] = React.useState(true);
    // 
    const { 
        loginStatus,
        actionLogin
    } = React.useContext(RootContext)
    React.useEffect(() => {
        checkToken();
    }, [loginStatus]);
    // 
    const checkToken = async () => {
        var token = '';
        try {
            const res = await AsyncStorage.getItem('Token');
            if (res !== null) {
                token = res;
            }
        } catch (e) {
            console.log(e);
        }
        if (token.length > 0) {
            actionLogin(true);   
        } 
        setToken(token);
        setLoading(false);
    }

    return (
        <>
            {
                loading ? null : (
                    <Navigator>
                        { token.length > 0 ? NeedLogin() : WithoutLogin()}
                    </Navigator>
                )
            }
        </>
    )
};

export default Navigation;
