import { day } from './../screens/TodoApps/todo.model';

const todoReducer = (state, action) => {
    switch (action.type) {
        case 'HAPUS':
            var obj = state.filter((_, i) => i !== action.value);
            return obj;
            break;
        case 'SIMPAN':
            var obj = state;
            if (action.value.length > 0) {
                obj.push({
                    text: action.value,
                    date: day(),
                });
            }
            return obj;
            break;
        default:
            return state;
    }
}

export default todoReducer;