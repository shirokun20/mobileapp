import { warna } from '../../style';

const dataMenu = [{
    text: 'E-mail',
    icon: 'email',
    color: warna.orange,
}, {
    text: 'Account',
    icon: 'account-circle',
    color: warna.blue,
}, {
    text: 'Gift',
    icon: 'card-giftcard',
    color: warna.green,
}, {
    text: 'Whistlist',
    icon: 'add-shopping-cart',
    color: warna.red,
}, {
    text: 'Todo App',
    icon: 'list',
    color: warna.grey,
}, {
    text: 'News App',
    icon: 'list-alt',
    color: warna.black,
}];

export default dataMenu;
