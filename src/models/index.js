import dataMenu from './MenuHome/menuHome.model';
import dataBottomTab from './BottomTabMenu/bottomTabMenu.model';

export { 
    dataMenu,
    dataBottomTab
}