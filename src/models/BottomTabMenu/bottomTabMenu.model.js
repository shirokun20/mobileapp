const dataBottomTabMenu = [{
    text: 'E-mail',
    icon: 'email',
}, {
    text: 'Account',
    icon: 'account-circle',
}, {
    text: 'Gift',
    icon: 'card-giftcard',
}, {
    text: 'Whistlist',
    icon: 'add-shopping-cart',
}, {
    text: 'Todo App',
    icon: 'list',
}, {
    text: 'News App',
    icon: 'list-alt',
}];

export {
    dataBottomTabMenu
}