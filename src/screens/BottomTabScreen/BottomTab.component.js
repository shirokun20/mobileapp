import React from 'react';

import { View, Text, TouchableOpacity, Animated } from 'react-native';
import { Easing } from 'react-native-reanimated';
import { warna } from '../../style';

const BottomTabComponent = ({ state, descriptors, navigation }) => {
    const focusedOptions = descriptors[state.routes[state.index].key].options;

    if (focusedOptions.tabBarVisible === false) {
        return null;
    }

    return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: warna.white,
            elevation: 1.8,
            paddingBottom: 15,
            paddingTop: 10,
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
        }}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const icon = options.tabBarIcon({
                    color: isFocused ? warna.appGreen2Color : warna.black
                });

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                return (
                    <TouchableOpacity
                        key={index}
                        activeOpacity={0.8}
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={{
                            flex: 1,
                            marginLeft: options.marginLeft,
                            marginRight: options.marginRight,
                        }}
                    >
                        <Animated.View style={{
                            alignItems: 'center',
                        }}>
                            {icon}
                            <Animated.Text style={{
                                color: isFocused ? warna.appGreen2Color : '#222',
                                fontSize: 17,
                                fontWeight: isFocused ? 'bold' : 'normal',
                                textAlign: 'center',
                            }}>
                                {label}
                            </Animated.Text>
                        </Animated.View>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
}

export default BottomTabComponent;
