import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BillboardActivity, BukuSaku, OnlineLearning, Account, Sedekah } from '../AppUtama';
import BottomTabComponent from './BottomTab.component';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { warna } from '../../style';
import { SEDEKAH_2_IMG } from './../../assets/images'
//
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const Tab = createBottomTabNavigator();
// Menu Bottom Tab
const MenuBTab = [{
    text: 'Ba',
    desc: 'Billboard',
    component: BillboardActivity,
    icon: ({ color = warna.black }) => (
        <MaterialIcons
            name={'dashboard'}
            size={30}
            color={color}
        />
    ),
    marginRight: 0,
    marginLeft: 0,
}, {
    text: 'Bs',
    desc: 'Buku Saku',
    component: BukuSaku,
    icon: ({ color = warna.black }) => (
        <MaterialIcons
            name={'fact-check'}
            size={30}
            color={color}
        />
    ),
    marginRight: 25,
    marginLeft: 0,
}, {
    text: 'Ol',
    desc: 'Learning',
    component: OnlineLearning,
    icon: ({ color = warna.black }) => (
        <MaterialCommunityIcons
            name={'pencil-box'}
            size={30}
            color={color}
        />
    ),
    marginRight: 0,
    marginLeft: 25,
}, {
    text: 'Account',
    desc: 'Account',
    component: Account,
    icon: ({ color = warna.black }) => (
        <MaterialIcons
            name={'person'}
            size={30}
            color={color}
        />
    ),
    marginRight: 0,
    marginLeft: 0,
},
    // {
    //     text: 'Sb',
    //     desc: `Sedekah Berjama'ah`,
    //     component: Sedekah
    // },
];


const ChildTab = ({
    navigation
}) => (
    <View style={{
        position: 'absolute',
        bottom: 35,
        alignSelf: 'center',
        height: 70,
        width: 70
    }}>
        <TouchableOpacity style={{
            backgroundColor: warna.appGreen2Color,
            elevation: 1.9,
            height: 65,
            width: 65,
            borderRadius: 150,
            alignItems: 'center',
            justifyContent: 'center',
        }} activeOpacity={0.9}>
            <Image
                source={SEDEKAH_2_IMG}
                style={{
                    width: 50,
                    height: 50,
                    resizeMode: 'contain',
                    tintColor: warna.white,
                }}
            />
        </TouchableOpacity>
    </View>
)


const BottomTabScreen = (props) => {
    return (
        <>
            <Tab.Navigator
                tabBar={props => <BottomTabComponent {...props} />}
                initialRouteName={MenuBTab[0].text}>
                {
                    MenuBTab.map((e, index) => (
                        <Tab.Screen
                            key={index}
                            name={e.text}
                            options={{
                                tabBarLabel: e.desc,
                                tabBarIcon: ({ color }) => e.icon({ color }),
                                marginLeft: e.marginLeft,
                                marginRight: e.marginRight,
                            }}
                            component={e.component}
                        />
                    ))
                }
            </Tab.Navigator>
            <ChildTab {...props} />
        </>
    )
};

export default BottomTabScreen;
