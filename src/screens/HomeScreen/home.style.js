import { StyleSheet } from 'react-native';
import { warna } from '../../style';

const homeStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: warna.white,
    },
    content: {
        marginTop: 40,
        marginHorizontal: 24,
        flex: 1,
    },
    logoView: {
        marginTop: 13,
        alignItems: 'center'
    },
    textJudul: {
        fontSize: 20 * 2,
        fontWeight: '500',
        color: warna.black
    },
    buttonView: {
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        alignItems: 'center',
        width: '100%',
        paddingBottom: 38,
        paddingHorizontal: 20,
        backgroundColor: warna.white,
        paddingTop: 5,
    },
    helloView: {
        marginTop: 15,
        marginBottom: 5 * 1,
    },
    menuView: {
        flex: 1,
        marginBottom: 20,
        paddingHorizontal: 20,
        alignItems: 'center',
    },
    cardView: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        marginBottom: 5,
        height: 50 * 3,
        elevation: 1,
    },
    iconColor: {
        color: warna.white
    },
    textMenu: {
        fontSize: 16 * 1.2,
        fontWeight: 'bold'
    }
});

export {
    homeStyle,
}