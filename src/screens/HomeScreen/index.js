import React from 'react';
import { FlatList, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { warna } from '../../style';
import { dataMenu } from '../../models';
import { ButtonCustom, Logo } from '../../components';
import { homeStyle } from './home.style';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import { API_LINK } from '../../api';
import { RootContext } from '../../Context';
// 
const LogoContent = () => (
    <View style={homeStyle.logoView}>
        <Logo
            width={80}
            height={80}
        />
    </View>
);

const ButtonView = ({ navigation }) => {
    
    const { actionLogin }  = React.useContext(RootContext)

    const logOut = async () => {
        try {
            await AsyncStorage.clear();
            actionLogin(false);
        } catch (e) {
            console.log(e);
        }
    }

    const _logOut = () => navigation.reset({
        index: 0,
        routes: [{ name: 'loginScreen' }],
    });

    return (
        <View style={homeStyle.buttonView}>
            <ButtonCustom
                text={'Logout'}
                color={warna.red}
                onPress={logOut}
            />
        </View>
    )
};

const HelloView = ({
    text = '',
}) => {
    return (
        <View style={homeStyle.helloView}>
            <Text style={[homeStyle.textJudul, {
                fontWeight: 'bold',
            }]}>Home</Text>
            <Text style={[homeStyle.textJudul, {
                fontSize: 20 * 1.5,
                top: -10,
            }]}>Hello, {text}</Text>
        </View>
    )
}

const RenderItem = ({ item, navigation }) => {
    return (
        <View style={[homeStyle.menuView]}>
            <TouchableOpacity style={[{
                backgroundColor: item.color,
            }, homeStyle.cardView]} activeOpacity={0.8} onPress={() => {
                if (item.text == 'Todo App') navigation.navigate('TodoApps');
                if (item.text == 'News App') navigation.navigate('NewsScreen');
            }}>
                <MaterialIcons
                    name={item.icon}
                    size={50}
                    style={homeStyle.iconColor}
                />
            </TouchableOpacity>
            <Text style={homeStyle.textMenu}>{item.text}</Text>
        </View>
    )
}

const MenuView = ({ navigation }) => {
    return (
        <View style={{
            height: '75%'
        }}>
            <FlatList
                data={dataMenu}
                numColumns={2}
                renderItem={(item) => <RenderItem {...item} navigation={navigation} />}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    )
}

const HomeScreen = (props) => {

    const [name, setName] = React.useState('');

    React.useEffect(() => {
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem('Token');
                if (!token) return props.navigation.reset({
                    index: 0,
                    routes: [{ name: 'loginScreen' }],
                });
                return getProfile(token);
            } catch (e) {
                console.log(e);
            }
        }
        getToken();
    }, []);

    const getProfile = async (token) => {
        var text = '';
        try {
            const { data } = await Axios.get(`${API_LINK.api}/profile/get-profile`, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            });
            if (data.response_code == '00') {
                const {
                    name = ''
                } = data.data.profile;
                text = name;
            }
        } catch (e) {
            console.log(e);
        }
        setName(text);
    }


    return (
        <View style={homeStyle.container}>
            <StatusBar
                barStyle="dark-content"
                translucent
                backgroundColor={warna.transparent}
            />
            <View style={homeStyle.content}>
                <LogoContent />
                <HelloView text={name} />
                <MenuView {...props} />
            </View>
            <ButtonView {...props} />
        </View>
    )
};

export default HomeScreen;
