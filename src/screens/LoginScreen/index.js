import React from 'react';
import { StatusBar, View, Text, TouchableOpacity, SafeAreaView, Keyboard } from 'react-native';
import { AlertWaiting, ButtonCustom, ButtonTextOnly, InputText, Logo } from '../../components';
import { warna } from '../../style';
import { loginStyle } from './login.style';
import Axios from 'axios';
import { API_LINK } from '../../api';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { RootContext } from '../../Context';
// 
const LogoContent = () => (
    <View style={loginStyle.logoView}>
        <Logo
            width={80}
            height={80}
        />
    </View>
);
// 
const WelcomeContent = () => (
    <View style={loginStyle.welcomeView}>
        <Text style={loginStyle.textWelcome}>Selamat Datang{'\n'}Pemimpin Umat</Text>
    </View>
);
// 
const LupaPwdDanDaftarContent = (props) => {
    return (
        <View style={loginStyle.lpdContent}>
            <View style={loginStyle.daftarView}>
                <Text style={[loginStyle.textDaftar1, loginStyle.textFontSize]}>Belum punya akun? </Text>
                <ButtonTextOnly
                    text="Daftar"
                />
            </View>
            <View>
                <ButtonTextOnly
                    text="Lupa Password?"
                />
            </View>
        </View>
    )
}
// 
const ButtonView = ({
    onPress,
}) => (
    <View style={loginStyle.buttonView}>
        <ButtonCustom
            text={'Masuk'}
            onPress={onPress} 
        />
    </View>
);

const InputView = ({
    marginBottom = 30,
    field = '',
    placeholder = '',
    value = '',
    secure = false,
    onChangeText,
}) => (
    <View style={{
        marginBottom,
    }}>
        <InputText
            placeholder={placeholder}
            field={field}
            onChangeText={(text) => onChangeText(text)}
            secure={secure}
            value={value}
        />
    </View>
)

const LoginScreen = (props) => {
    //
    const [email, setEmail] = React.useState('');
    const [password, setPasword] = React.useState('');
    const [modal, setModal] = React.useState({
        waitingAlert: false,
    });
    const { actionLogin } = React.useContext(RootContext);

    const loginCheck = async () => {
        var error = true;
        var message = '';
        try {
            const {data} = await Axios.post(`${API_LINK.api}/auth/login`, {
                email,
                password,
            });
            error = false;
            if (data.response_code == '00') {
                saveToken(data.data.token);
            } else {
                message = 'Akun tidak ditemukan!';
            }
        } catch (e) {
            console.log(e);
            message = 'Akun tidak ditemukan!';
        }

        if (error) {
            alert(message);
            setModal({...modal, waitingAlert: false})
        }
    }

    const saveToken = async (token = '') => {
        try {
           await AsyncStorage.setItem('Token', token);
           actionLogin(true);
           setModal({...modal, waitingAlert: false});
        } catch(e) {
            console.log(e);
        }
    }

    return (
        <View style={loginStyle.container}>
            <StatusBar
                barStyle="dark-content"
                translucent
                backgroundColor={warna.transparent}
            />
            <TouchableOpacity
                style={loginStyle.content}
                onPress={() => Keyboard.dismiss()}
                activeOpacity={1}>
                {/* Logo */}
                <LogoContent />
                {/* Welcome Content */}
                <WelcomeContent />
                {/* Form Content*/}
                <SafeAreaView>
                    <InputView
                        field={'Email'}
                        value={email}
                        onChangeText={text => setEmail(text)}
                    />
                    <InputView
                        field={'Password'}
                        value={password}
                        secure={true}
                        marginBottom={5}
                        onChangeText={text => setPasword(text)}
                    />
                </SafeAreaView>
                {/* LPD */}
                <LupaPwdDanDaftarContent {...props} />
                {/* Buttton */}
                <ButtonView 
                    {...props}
                    onPress={() => {
                        if (email.length < 1) {
                            alert('Email tidak boleh kosong!')
                        } else if (password.length < 1) {
                            alert('Password tidak boleh kosong!')
                        } else {
                            setModal({...modal, waitingAlert: true});
                            setTimeout(() => loginCheck(), 500);
                        }
                    }}
                />
            </TouchableOpacity>
            <AlertWaiting
                show={modal.waitingAlert}
            />
        </View>
    )
};

export default LoginScreen;