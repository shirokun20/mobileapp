import { StyleSheet } from 'react-native';
import { warna } from '../../style';

const loginStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: warna.white,
    },
    content: {
        marginTop: 40,
        marginHorizontal: 24,
        flex: 1,
    },
    logoView: {
        marginTop: 13,
        alignItems: 'center'
    },
    welcomeView: {
        marginTop: 27,
        marginBottom: 25 * 2,
    },
    textWelcome: {
        fontSize: 20 * 2,
        fontWeight: '500',
        color: warna.black
    },
    lpdContent: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    daftarView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textDaftar1: {
        color: warna.grey
    },
    textLPD: {
        color: warna.red
    },
    textFontSize: {
        fontSize: 15,
    },
    buttonView: {
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        alignItems: 'center',
        width: '100%',
        marginBottom: 30,
    }
});

export {
    loginStyle
}