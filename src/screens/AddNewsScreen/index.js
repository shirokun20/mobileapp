import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { AlertCustom, ButtonCustom, InputText, AlertWaiting } from '../../components';
import { warna } from '../../style';
// 
import Axios from 'axios';
import { API_LINK } from '../../api';

const AddNewsScreen = ({ navigation }) => {

    const [form, setForm] = React.useState({
        title: '',
        value: '',
    });

    const [modal, setModal] = React.useState({
        error: true,
        title: '',
        message: '',
        show: false,
        waitingAlert: false,
    });

    const simpanData = () => {
        const {
            title,
            value,
        } = form;

        Axios.post(API_LINK.utama, JSON.stringify({
            title,
            value
        }), {
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(({ data }) => {
            if (data.success == 200) {
                setModal({ ...modal, waitingAlert: false});
                setTimeout(() => {
                    setModal({
                        ...modal,
                        show: true,
                        message: 'Berhasil menambah data news!',
                        title: 'Berhasil!',
                        error: false
                    });
                    modalSetHide(true);
                }, 100);
            }
        }).catch((error) => {
            setModal({
                ...modal,
                show: true,
                message: 'Gagal menambah news!',
                title: 'Error!',
                error: true
            });
            modalSetHide();
        });
    }

    const modalSetHide = (success = false) => {
        setTimeout(() => {
            setModal({ ...modal, show: false });
        }, 500);
        if (success) setTimeout(() => {
            navigation.goBack();
        }, 1000);
    }

    return (
        <View style={{
            backgroundColor: warna.white,
            flex: 1,
            paddingTop: 5,
        }}>
            <ScrollView contentContainerStyle={{
                paddingHorizontal: 10,
            }} showsVerticalScrollIndicator={false}>
                <InputText
                    field={'Title'}
                    placeholder={'Masukan Title..'}
                    onChangeText={(text) => {

                        setForm({ ...form, title: text });
                    }}
                    value={form.title} />
                <View style={{ height: 10 }} />
                <InputText
                    field={'Value'}
                    placeholder={'Masukan Value..'}
                    multiline={true}
                    onChangeText={(text) => {

                        setForm({ ...form, value: text });
                    }}
                    value={form.value} />
                <View style={{ height: 20 }} />
                <ButtonCustom
                    text="SIMPAN"
                    onPress={() => {
                        if (form.title.length < 1) {
                            setModal({
                                ...modal,
                                show: true,
                                message: 'Title harap di isi!',
                                title: 'Error!',
                                error: true
                            });
                            modalSetHide();
                        } else if (form.value.length < 1) {
                            setModal({
                                ...modal,
                                show: true,
                                message: 'Value harap di isi!',
                                title: 'Error!',
                                error: true
                            })
                            modalSetHide();
                        } else {
                            setModal({ ...modal, waitingAlert: true })
                            setTimeout(() => simpanData(), 500);
                        }
                    }}
                />
                <View style={{ height: 40 }} />
            </ScrollView>
            <AlertCustom
                show={modal.show}
                title={modal.title}
                error={modal.error}
                message={modal.message}
            />
            <AlertWaiting
                show={modal.waitingAlert}
            />
        </View>
    )
};

export default AddNewsScreen;
