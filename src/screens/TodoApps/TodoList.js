import React from 'react';
import { FlatList, Keyboard, Text, View } from 'react-native';
import { todoStyle } from './todo.style';
import { TodoButton, TodoInput, TodoItem } from './todo.component';
import { RootContext } from '../../Context';

const TodoList = (props) => {

    const { dispatch, todoData } = React.useContext(RootContext);
    const [todoInput, setTodoInput] = React.useState('');

    const renderItem = ({
        item, index
    }) => (<TodoItem onDelete={() => dispatch({
        type: 'HAPUS',
        value: index,
    })} {...item} />);

    return (
        <View style={todoStyle.container}>
            <View style={todoStyle.childView}>
                <Text style={todoStyle.text}>Masukan Todolist</Text>
                <View style={todoStyle.formView}>
                    <TodoInput
                        onChangeText={(text) => setTodoInput(text)}
                        value={todoInput}
                    />
                    <TodoButton
                        onPress={() => {
                            dispatch({
                                type: 'SIMPAN',
                                value: todoInput,
                            });

                            if (todoInput.length > 0) {
                                setTodoInput('');
                                Keyboard.dismiss();
                            }
                        }}
                    />
                </View>
                <View style={{
                    flex: 1,
                }}>
                    <FlatList
                        data={todoData}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={renderItem}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </View>
        </View>
    );
};

export default TodoList;