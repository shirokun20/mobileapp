import { StyleSheet } from 'react-native';
import { warna } from '../../style';

const todoStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: warna.white
    },
    childView: {
        // marginTop: 40,
        flex: 1,
        paddingHorizontal: 5,
    },
    text: {
        marginTop: 10,
    },
    formView: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    inputView: {
        paddingVertical: 10,
        width: '90%',
        height: 40,
        borderWidth: 1,
        paddingHorizontal: 5,
        marginRight: 5
    },
    buttonView: {
        backgroundColor: '#00ffff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 40,
    },
    cardView: {
        borderWidth: 3,
        borderColor: warna.borderColor,
        backgroundColor: warna.white,
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 10,
        justifyContent: 'space-between',
        marginBottom: 10,
        borderRadius: 10,
    }
});

export {
    todoStyle,
}