import React from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import { todoStyle } from './todo.style';
import { AntDesign } from '@expo/vector-icons';
import { warna } from '../../style';

const TodoItem = ({
    date = '',
    text = '',
    onDelete,
}) => (
    <TouchableOpacity style={todoStyle.cardView} activeOpacity={1}>
        <View style={{
            width: '90%',
        }}>
            <Text style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: warna.grey,
            }}>{date}</Text>
            <Text style={{
                fontSize: 20,
                color: warna.grey,
            }}>{text}</Text>
        </View>
        <TouchableOpacity style={{
            flex: 1,
            alignItems: 'flex-end',
        }} onPress={() => onDelete()}>
            <AntDesign
                name="delete"
                size={30}
            />
        </TouchableOpacity>
    </TouchableOpacity>
);


const TodoButton = ({
    onPress,
}) => (
    <TouchableOpacity style={todoStyle.buttonView} onPress={onPress}>
        <AntDesign
            name="plus"
            size={26}
        />
    </TouchableOpacity>
);

const TodoInput = ({
    onChangeText,
    value,
}) => (
    <View style={todoStyle.inputView}>
        <TextInput
            placeholder="Input Here"
            style={{
                padding: 0,
            }}
            value={value}
            onChangeText={onChangeText}
        />
    </View>
);

export {
    TodoItem,
    TodoButton,
    TodoInput
}