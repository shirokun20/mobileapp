var bulanData = [
    'Januari', 
    'Februari', 
    'Maret', 
    'Mei', 
    'Juni', 
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember' 
];

const day = () => {
    var date = new Date();
    var bulan = bulanData[date.getMonth()];
    var tahun = date.getFullYear();
    var tanggal = date.getDate();
    var jam = date.getHours();
    var menit = date.getMinutes();
    return `${tanggal}/${bulan}/${tahun} ${jam}:${menit}`;
};

export {
    bulanData,
    day
}