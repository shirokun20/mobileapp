import React from 'react';
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { warna } from '../../style';
import Axios from 'axios';
import { API_LINK } from '../../api';
import { AntDesign } from '@expo/vector-icons';
import { AlertConfirm } from '../../components';

const NewsLoading = () => (
    <View style={newsStyle.loadingContainer}>
        <ActivityIndicator
            color={warna.black}
            size={70}
        />
        <Text style={newsStyle.textLoading}>Tunggu Sebentar!</Text>
    </View>
);

const NewsItem = ({
    title = '',
    value = '',
    index = 0,
    onDelete
}) => (
    <View style={[{
        marginTop: index > 0 ? 0 : 5,
    }, newsStyle.parentCard]}>
        <TouchableOpacity style={newsStyle.card} activeOpacity={0.9}>
            <View style={{
                flexDirection: 'row',
            }}>
                <View style={{
                    width: '80%',
                }}>
                    <Text style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                    }}>{title}</Text>
                    <Text>{value}</Text>
                </View>
                <TouchableOpacity style={{
                    flex: 1,
                    alignItems: 'flex-end',
                }} onPress={() => onDelete()} activeOpacity={0.8}>
                    <AntDesign
                        name="delete"
                        size={30}
                    />
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    </View>
)

const NewsData = ({
    onRefresh,
    refresh = false,
    data,
    deleteAction,
}) => {
    return (
        <View style={{
            flex: 1,
        }}>
            <Text style={newsStyle.textNews}>List Berita</Text>
            <FlatList
                data={data}
                keyExtractor={(_, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={({ item, index }) => (
                    <NewsItem {...item} index={index} onDelete={() => deleteAction(item.id)} />
                )}
                refreshControl={<RefreshControl
                    colors={[warna.red]}
                    refreshing={refresh}
                    onRefresh={onRefresh}
                />}
            />
        </View>
    )
}

NewsData.defaultProps = {
    onRefresh: () => { },
    data: [],
    deleteAction: () => { },
}

const NewsScreen = (props) => {
    //
    const [data, setData] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [refresh, setRefresh] = React.useState(false);
    const [deleteConfirm, setOnDeleteConfirm] = React.useState(false);
    const [deleteID, setDeteleID] = React.useState('');
    //
    React.useEffect(() => {
        const cancelToken = Axios.CancelToken;
        const source = cancelToken.source();
        setAwal();
        return () => source.cancel();
    }, []);

    const setAwal = () => setTimeout(() => {
        getData();
    }, 500);
    //
    const getData = async () => {
        var objData = [];
        try {
            const {
                data,
            } = await Axios.get(API_LINK.utama);
            if (data.status == 200) {
                objData = data.data;
            }
        } catch (e) {
            console.log(e);
        }
        setData(objData);
        setLoading(false);
        setRefresh(false);
    }
    //
    const deleteData = () => {
        Axios.delete(`${API_LINK.utama}/${deleteID}`).then(({
            data
        }) => {
            setTimeout(() => {
                getData();
            }, 1000);
        }).catch((e) => {
            console.log(e)
        });
    }
    //
    return (
        <View style={newsStyle.container}>
            { loading ? (
                <NewsLoading />
            ) : <NewsData
                    refresh={refresh}
                    onRefresh={() => {
                        setRefresh(true);
                        setTimeout(() => {
                            setLoading(true);
                            setRefresh(false);
                        }, 1000);

                        setTimeout(() => getData(), 2000);
                    }}
                    data={data}
                    deleteAction={(id) => {
                        setOnDeleteConfirm(true);
                        setDeteleID(id);
                    }}
                />}
            <AlertConfirm 
                title="Hapus"
                message="Apakah yakin akan menghapus data ini?"
                show={deleteConfirm}
                onBackDrop={() => {
                    setDeteleID('');
                    setOnDeleteConfirm(false)
                }}
                onNo={() => {
                    setDeteleID('');
                    setOnDeleteConfirm(false);
                }}
                onYes={() => {
                    setOnDeleteConfirm(false);
                    setLoading(true);
                    deleteData();
                }}
            />
        </View>
    )
};

const newsStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: warna.white,
    },
    card: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: warna.white,
        elevation: 2,
        borderRadius: 10,
        width: '100%',
    },
    parentCard: {
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    textNews: {
        fontWeight: 'bold',
        fontSize: 25,
        paddingHorizontal: 10,
        marginTop: 5,
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textLoading: {
        marginTop: 10,
        fontSize: 25,
        fontWeight: 'bold'
    }
})

export default NewsScreen;
