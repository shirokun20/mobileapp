import React from 'react';
import { StatusBar, StyleSheet, Text, View } from 'react-native';
import { LogoAnimation } from '../../components';
import { warna } from '../../style';
import AsyncStorage from '@react-native-async-storage/async-storage'

const SplashScreen = ({ navigation, }) => {

    // Senagaja tidak pakai useEffect fi Splashscreen karena
    // Di Animation Logonya ada use Effect 
    // kan ketika animation muncul lalu selesai, maka ada ada kondisi dimana check untuk localStorage
    // Kenapa dilakukan seperti itu, untuk menerapkan callback :-)
    // Lalu sebagai percobaan :-D

    const [animationIn, setAnimationIn] = React.useState(true);
    const [token, setToken] = React.useState('');
    
    const Route = (name = '') => navigation.reset({
        index: 0,
        routes: [{
            name,
        }]
    });
    // Ketika Animation finish
    const checkToken = async () => {
        var token = '';
        try {
            const res = await AsyncStorage.getItem('Token');
            if (res !== null) {
                token = res;
            }
        } catch (e) {
            console.log(e);
        } 
        setToken(token);
        // setAnimationIn false untuk animation keluar
        setTimeout(() => {
            setAnimationIn(false);
        }, 500)
    }

    return (
        <View style={localStyle.container}>
            <StatusBar
                barStyle="dark-content"
                translucent
                backgroundColor={warna.transparent}
            />
            <View style={localStyle.content}>
                <LogoAnimation
                    width={200}
                    height={200}
                    onFinished={() => {
                        if (token.length > 0) {
                            Route('homeScreen');
                        } else {
                            Route('loginScreen');
                        }
                    }}
                    animationIn={animationIn}
                    onAnimationInFinish={checkToken}
                />
            </View>
        </View>
    )
};

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: warna.white
    },
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default SplashScreen;
