import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainNavigation from './navigation';
import { RootContext } from './Context';
import { day } from './screens/TodoApps/todo.model';
import { todoReducer } from './reducers';

var dataSementara = [{
    text: 'Membuat login screen',
    date: day()
}, {
    text: 'Membuat home page',
    date: day()
},];

const Provider = ({ children }) => {
    const [todoData, dispatch] = React.useReducer(todoReducer, dataSementara);
    const [loginStatus, setLoginStatus] = React.useState(false);
    const providerData = {
        todoData,
        dispatch,
        loginStatus,
        actionLogin: (value = false) => {
            setLoginStatus(value);
        } 
    }

    return (
        <RootContext.Provider value={providerData}>
            {children}
        </RootContext.Provider>
    );
}

const Core = () => {

    return (
        <NavigationContainer>
            <Provider>
                <MainNavigation />
            </Provider>
        </NavigationContainer>
    );
};

export default Core;
