import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainNavigation from './navigation';

const Core = () => {

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    );
};

export default Core;
