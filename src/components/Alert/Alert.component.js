import React from 'react';
import { Dimensions, Modal, Text, View } from 'react-native';
import { warna } from '../../style';

const AlertCustom = ({
    show = false,
    title = '',
    message = '',
    error = false,
}) => {
    const { height, width } = Dimensions.get('window');
    return (
        <Modal visible={show} animationType="fade" transparent>
            <View 
                style={{
                    opacity: 0.5,
                    height: '100%',
                    width: '100%',
                    backgroundColor: warna.black,
                }}
            />
            <View style={{
                top: height / 2.5,
                position: 'absolute',
                alignSelf: 'center',
                width: width / 2,
                borderRadius: 10,
                backgroundColor: warna.white,
            }}>
                <View style={{
                    backgroundColor: error ? warna.red : warna.green,
                    alignItems: 'center',
                    padding: 10,
                    borderTopStartRadius: 10,
                    borderTopEndRadius: 10,
                }}>
                    <Text style={{
                        color: warna.white
                    }}>{title}</Text>
                </View>
                <View style={{
                    alignItems: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 10,
                }}>
                    <Text style={{
                        fontSize: 20,
                    }}>
                        {message}
                    </Text>
                </View>
            </View>       
        </Modal>
    )
};

AlertCustom.defaultProps = {
    onHideAlert: () => { },
}

export default AlertCustom;
