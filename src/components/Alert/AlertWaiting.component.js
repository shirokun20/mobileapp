import React from 'react';
import { ActivityIndicator, Dimensions, Modal, Text, View } from 'react-native';
import { warna } from '../../style';

const AlertWaiting = ({
    show = false,
}) => {
    const { height, width } = Dimensions.get('window');
    return (
        <Modal visible={show} animationType="fade" transparent>
            <View 
                style={{
                    opacity: 0.5,
                    height: '100%',
                    width: '100%',
                    backgroundColor: warna.black,
                }}
            />
            <View style={{
                top: height / 2.5,
                position: 'absolute',
                alignSelf: 'center',
                width: width / 2,
                borderRadius: 10,
                backgroundColor: warna.white,
            }}>
                <View style={{
                    alignItems: 'center',
                    paddingVertical: 30,
                    paddingHorizontal: 10,
                }}>
                    <ActivityIndicator 
                        color={warna.black}
                        size={30}
                    />
                    <Text style={{
                        fontSize: 20,
                        marginTop: 20,
                    }}>Tunggu Sebentar!</Text>
                </View>
            </View>       
        </Modal>
    )
};

export default AlertWaiting;
