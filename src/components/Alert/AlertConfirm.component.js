import React from 'react';
import { Dimensions, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { warna } from '../../style';

const { height, width } = Dimensions.get('window');

const AlertButton = ({
    onPress,
    color = warna.green,
    text = ''
}) => (
    <TouchableOpacity style={[{
        backgroundColor: color,
    }, AcStyle.buttonAlert]} activeOpacity={0.8} onPress={onPress}>
        <Text style={AcStyle.textButtonAlert}>{text}</Text>
    </TouchableOpacity>
)

const AlertConfirm = ({
    show = false,
    title = '',
    message = '',
    onYes,
    onNo,
    onBackDrop,
}) => {

    const Footer = (
        <View style={AcStyle.footerContainer}>
            <View style={{
                flex: 1,
            }}>
                <AlertButton 
                    onPress={onYes}
                    color={warna.green}
                    text={'Ya'}
                />
            </View>
            <View style={{
                width: 10,
            }}/>
            <View style={{
                flex: 1,
            }}>
                <AlertButton 
                    onPress={onNo}
                    color={warna.red}
                    text={'Tidak'}
                />
            </View>
        </View>
    );

    return (
        <Modal visible={show} animationType="fade" transparent>
            <TouchableOpacity 
                style={AcStyle.backdrop}
                activeOpacity={0.8}
                onPress={onBackDrop}
            />
            <View style={AcStyle.container}>
                <View style={AcStyle.card}>
                    <View style={AcStyle.header}>
                        <Text style={AcStyle.headerText}>{title}</Text>
                    </View>
                    <View style={AcStyle.cardContent}>
                        <Text style={AcStyle.cardText}>
                            {message}
                        </Text>
                    </View>
                </View>
                {Footer}
            </View>       
        </Modal>
    )
};

AlertConfirm.defaultProps = {
    onYes: () => { },
    onNo: () => { },
    onBackDrop: () => { },
}

const AcStyle = StyleSheet.create({
    backdrop: {
        opacity: 0.8,
        height: '100%',
        width: '100%',
        backgroundColor: warna.black,
    },
    buttonAlert: {
        alignItems: 'center',
        paddingVertical: 10,
        width: '100%',
        borderRadius: 10,
        elevation: 2,
    },
    textButtonAlert: {
        color: warna.white,
        fontWeight: 'bold',
        fontSize: 17,
    },
    container: {
        top: height / 3.5,
        position: 'absolute',
        alignSelf: 'center',
        width: width / 1.7,
    },
    card: {
        width: '100%',
        elevation: 2,
        backgroundColor: warna.white,
        borderRadius: 10,
        marginBottom: 10,
    },
    header: {
        backgroundColor: warna.red,
        alignItems: 'center',
        padding: 10,
        borderTopStartRadius: 10,
        borderTopEndRadius: 10,
    },
    headerText: {
        color: warna.white,
        fontSize: 17
    },
    cardContent: {
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 10,
    },
    cardText: {
        fontSize: 20,
        textAlign: 'center',
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    }
})

export default AlertConfirm;