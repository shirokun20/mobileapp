import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { warna } from '../../style';

const ButtonTextOnly = ({
    color = warna.red,
    text = '',
    onPress,
    fontSize = 15
}) => (
    <TouchableOpacity activeOpacity={0.7} onPress={() => onPress()}>
        <Text style={[{
            color,
            fontSize
        }]}>{text}</Text>
    </TouchableOpacity>
);

ButtonTextOnly.defaultProps = {
    onPress: () => { },
}

export default ButtonTextOnly;
