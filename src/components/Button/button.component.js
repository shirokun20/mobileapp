import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { warna } from '../../style';

const ButtonCustom = ({
    color = warna.blue,
    text = '',
    onPress,
}) => (
    <TouchableOpacity
        style={{
            backgroundColor: color,
            alignItems: 'center',
            borderRadius: 150 / 2,
            paddingVertical: 12,
            width: '100%',
            elevation: 1,
        }}
        activeOpacity={0.8}
        onPress={() => onPress()}>
        <Text style={{
            color: warna.white,
            fontSize: 20,
        }}>{text}</Text>
    </TouchableOpacity>
);

ButtonCustom.defaultProps = {
    onPress: () => { }
}

export default ButtonCustom;
