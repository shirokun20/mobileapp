import React from 'react';
import { Animated, Easing, Image } from 'react-native';
import { LOGO_IMG } from '../../assets/images';
// logo untuk splashscreen :-D
const LogoAnimation = ({
    width = 100,
    height = 100,
    onFinished,
    onAnimationInFinish,
    animationIn = true,
}) => {
    // Aniamted 
    const LogoAnimated = React.useRef(new Animated.Value(0)).current;
    // 
    React.useEffect(() => {
        if (animationIn) {
            setInAnimation();
        } else {
            setOutAnimation();
        }
    }, [animationIn]);
    // 
    var timer_1 = 1000;
    //  
    const setInAnimation = (value = 1) => {
        LogoAnimated.setValue(0);
        Animated.sequence([
            Animated.timing(LogoAnimated, {
                toValue: value,
                duration: timer_1,
                useNativeDriver: true,
                easing: Easing.linear,
            }),
        ]).start(({ finished }) => {
            if (finished) {
                onAnimationInFinish()
            }
        });
    }

    const setOutAnimation = () => {
        LogoAnimated.setValue(1);
        Animated.sequence([
            Animated.timing(LogoAnimated, {
                toValue: 0,
                duration: timer_1,
                useNativeDriver: true,
                easing: Easing.linear,
            }),
        ]).start(({ finished }) => {
            if (finished) {
                onFinished(true)
            }
        });
    }
    //

    const LogoAnimatedRange = LogoAnimated.interpolate({
        inputRange: [0, 1],
        outputRange: [4, 1],
    });

    const spin = LogoAnimated.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
      })

    return (
        <Animated.Image
            style={{
                width,
                height,
                opacity: LogoAnimated,
                transform: [{
                    scale: LogoAnimatedRange,
                }, {
                    rotateZ: spin,
                }],
            }}
            source={LOGO_IMG}
        />
    )
};

LogoAnimation.defaultProps = {
    onFinished: () => {},
    onAnimationInFinish: () => {}
}

export default LogoAnimation;
