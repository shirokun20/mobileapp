import React from 'react';
import { Image } from 'react-native';
import { LOGO_IMG } from '../../assets/images';
// logo untuk splashscreen :-D
const Logo = ({
    width = 100,
    height = 100,
}) => {
    return (
        <Image
            style={{
                width,
                height,
            }}
            source={LOGO_IMG}
        />
    )
};

export default Logo;
