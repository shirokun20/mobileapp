import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { warna } from '../../style';

const InputText = ({
    field = '',
    placeholder = '',
    secure = false,
    onChangeText,
    value = '',
    multiline = false,
}) => {
    const [focus, setFocus] = React.useState(false);

    return (
        <View style={{
            width: '100%',
        }}>
            <Text style={[localStyle.text, {
                color: !focus ? warna.grey : warna.blue,
            }]}>{field}</Text>
            <View style={[localStyle.inputView, {
                borderColor: !focus ? warna.borderColor : warna.blue,
            }]}>
                <TextInput
                    secureTextEntry={secure}
                    placeholder={placeholder}
                    autoCapitalize={'none'}
                    onFocus={() => setFocus(true)}
                    onBlur={() => setFocus(false)}
                    onChangeText={(text) => onChangeText(text)}
                    style={[localStyle.input, {
                        color: !focus ? warna.grey : warna.blue,
                    }]}
                    value={value}
                    multiline={multiline}
                />
            </View>
        </View>
    )
};

InputText.defaultProps = {
    onChangeText: () => { },
}

const localStyle = StyleSheet.create({
    text: {
        fontSize: 20,
    },
    inputView: {
        borderWidth: 1,
        borderRadius: 13,
        paddingVertical: 10,
        width: '100%',
        alignItems: 'center',
        backgroundColor: warna.white,
        elevation: 1
    },
    input: {
        padding: 0,
        fontSize: 20,
        width: '95%',
    }
})

export default InputText;
